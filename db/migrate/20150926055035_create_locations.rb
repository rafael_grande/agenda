class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :day
      t.string :hour

      t.timestamps null: false
    end
  end
end
