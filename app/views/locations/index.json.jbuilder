json.array!(@locations) do |location|
  json.extract! location, :id, :day, :hour
  json.url location_url(location, format: :json)
end
