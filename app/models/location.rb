class Location < ActiveRecord::Base
    
    belongs_to :user
    
    validates_presence_of :day, :hour, :user_id
    
    def self.reservado(dia, hora)
        location = where (['day LIKE :dia AND hour LIKE :hora', :dia => "%#{dia}%", :hora => "%#{hora}%"])
        location.first
    
        #where('hour=? OR day=?', :hora, :dia)
    end
        
end
