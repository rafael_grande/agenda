require 'test_helper'

class UserTest < ActiveSupport::TestCase
    test "Validar usuario" do
         user = User.new
         user.name = "Rafael Grande"
         user.email = ""
         user.password_digest = "123"
         assert !user.save
    end
    
     test "Criar usuario" do
         user2 = User.new
         user2.name = "Rafael Grande"
         user2.email = "rafael@grande.com"
         user2.password_digest = "123"
         assert user2.save
    end
    
    
    
    test "validar email" do
        user3 = User.new(:name => "Rafael", :email => "rafa@grande.com", :password_digest => "123456")
        user3.save
        user4 = User.new(:name => "Rafa", :email => "rafa@grande.com", :password_digest => "654321")
         assert !user4.save
    end
end
