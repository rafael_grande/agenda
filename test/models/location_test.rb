require 'test_helper'

class LocationTest < ActiveSupport::TestCase
    test "Validar usuario da reserva" do
        user = User.new
        user.name = "Rafael Grande"
        user.email = "rafael@hotmail.com"
        user.password_digest = "123"
        user.save
        
        location = Location.new
        location.day = 1
        location.hour = 1
        location.user_id = user.id
        
        assert location.user.name == user.name
    end

end
